#### 使用python+Pyqt5库

## 2019.7.4完成

# udpchatroom.py  
程序入口，UDP群聊，接收发送上下线通知
双击列表内的用户，调用privatechar.py ,实现私聊功能
单击用户，点击发送文件按钮，调用tcpserver.py，实现TCP发送文件功能
收文件时调用tcpclient.py文件，实现TCP接收文件功能
点击截图按钮，提示输入文件名，将截取当前窗口并保存
点击天气按钮，将调用weather.py，显示天气信息（仅当有外网时可以使用）
# ui_udpchatroom.py
程序入口文件的UI

# privatechat.py
单人与单人通信，收发文件
其他功能调用函数与udpchatroom.py一致

# ui_privatechat.py
私聊UI

# tcpserver.py，tcpclient.py
通过TCP通信发送，接收文件

# ui_tcpserver.py ui_tcpclient.py
TCP发送文件接收文件的UI

# weather.py
仅当有外网是可以使用
通过网络爬虫爬取墨迹天气的天气信息并显示

# ui_weather.py
天气信息的UI

# /catchImg
保存截图的文件夹，若不存在则创建

# /image
程序图标文件夹，不可删除

# /weatherIcon
存放天气信息图片，若不存在则创建
