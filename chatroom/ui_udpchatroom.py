from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QIcon,QPalette,QBrush,QPixmap
import sys


class Ui_Form(object):

    def setupUi(self,Form):
        #窗口设置
        Form.setObjectName("Form")
        Form.resize(648, 430)
        Form.setWindowIcon(QIcon('image/cht.png'))
        Form.setStyleSheet("#Form{border-image:url(image/new.jpg)}")

        pal = QPalette()
        pal.setBrush(QPalette.Background, QBrush(QPixmap("./image/new.jpg")))
        Form.setPalette(pal)

        self.sendButton = QtWidgets.QPushButton(Form)
        self.sendButton.setGeometry(QtCore.QRect(290, 390, 93, 28))
        self.sendButton.setObjectName("sendButton")
        self.sendButton.setIcon(QIcon('image/send.png'))

        #接收框
        self.messageBrowser = QtWidgets.QTextBrowser(Form)
        self.messageBrowser.setGeometry(QtCore.QRect(10, 20, 371, 211))
        self.messageBrowser.setObjectName("messageBrowser")
        self.messageBrowser.setStyleSheet("#messageBrowser{background-color: pink}")
        #发送框
        self.messageEdit = QtWidgets.QTextEdit(Form)
        self.messageEdit.setGeometry(QtCore.QRect(10, 290, 371, 91))
        self.messageEdit.setObjectName("messageEdit")
        self.messageEdit.setStyleSheet("#messageEdit{background-color: pink}")

        self.horizontalLayoutWidget = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 220, 360, 80))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        #字体按钮
        self.fontButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.fontButton.setObjectName("fontButton")

        self.horizontalLayout_2.addWidget(self.fontButton)
        self.fontButton.setIcon(QIcon('image/fonts.png'))
        #颜色按钮
        self.colorButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.colorButton.setObjectName("colorButton")

        self.colorButton.setIcon(QIcon('image/colors.png'))
        self.horizontalLayout_2.addWidget(self.colorButton)
        #发送文件按钮
        self.sendFileBtn = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.sendFileBtn.setObjectName("sendFileBtn")
        self.sendFileBtn.setIcon(QIcon('image/document.png'))
        self.horizontalLayout_2.addWidget(self.sendFileBtn)
        #截图按钮
        self.catchpicBtn = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.catchpicBtn.setObjectName("catchpicBtn")
        self.catchpicBtn.setIcon(QIcon('image/catchpic.png'))
        self.horizontalLayout_2.addWidget(self.catchpicBtn)

        #天气按钮
        self.weatherButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.weatherButton.setObjectName("weatherButton")
        self.weatherButton.setIcon(QIcon('image/weather.png'))
        self.horizontalLayout_2.addWidget((self.weatherButton))

        self.tableWidget = QtWidgets.QTableWidget(Form)
        self.tableWidget.setGeometry(QtCore.QRect(400, 20, 231, 361))
        self.tableWidget.horizontalHeader().setDefaultSectionSize(66)
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setStyleSheet("#tableWidget{background-color: pink}")
        self.tableWidget.setObjectName("tableWidget")

        userNameItem = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, userNameItem)
        hostNameItem = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, hostNameItem)
        ipAddrItem = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, ipAddrItem)

        #退出按钮
        self.quitButton = QtWidgets.QPushButton(Form)
        self.quitButton.setGeometry(QtCore.QRect(540, 390, 93, 28))
        self.quitButton.setObjectName("quitButton")
        self.quitButton.setIcon(QIcon('image/quit.png'))

        #在线标签
        self.userNumLabel = QtWidgets.QLabel(Form)
        self.userNumLabel.setGeometry(QtCore.QRect(30, 400, 100, 15))
        self.userNumLabel.setObjectName("userNumLabel")
        self.userNumLabel.setStyleSheet("#userNumLabel{background-color: gold}")
        self.retranslateUi(Form)


    def retranslateUi(self, Form):

        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "聊天室"))
        self.colorButton.setText(_translate("Form", "颜色"))
        self.fontButton.setText(_translate("Form","字体"))
        self.sendFileBtn.setText(_translate("Form", "文件传输"))
        self.catchpicBtn.setText(_translate("Form", "截图"))
        self.weatherButton.setText(_translate("Form","天气"))

        userNameItem = self.tableWidget.horizontalHeaderItem(0)
        userNameItem.setText(_translate("Form", "用户名"))
        hostNameItem = self.tableWidget.horizontalHeaderItem(1)
        hostNameItem.setText(_translate("Form", "主机名"))
        ipAddrItem = self.tableWidget.horizontalHeaderItem(2)
        ipAddrItem.setText(_translate("Form", "IP"))
        self.quitButton.setText(_translate("Form", "退出"))
        self.userNumLabel.setText(_translate("Form", "在线用户:"))



if __name__ == '__main__':

    ui = Ui_Form()
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    ui.setupUi(widget)
    widget.show()
    sys.exit(app.exec_())