# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'private.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon,QPalette,QBrush,QPixmap

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(438, 440)
        Form.setWindowIcon(QIcon('image/cht.png'))
        Form.setWindowOpacity(0.8)

        pal = QPalette()
        pal.setBrush(QPalette.Background, QBrush(QPixmap("./image/second.jpg")))
        Form.setPalette(pal)

        #消息发送框
        self.messageEdit = QtWidgets.QTextEdit(Form)
        self.messageEdit.setGeometry(QtCore.QRect(20, 296, 401, 91))
        self.messageEdit.setObjectName("messageEdit")
        self.messageEdit.setFocus()
        self.messageEdit.setStyleSheet("#messageEdit{background-color: cornflowerblue}")
        #用户名称
        self.userNameLabel = QtWidgets.QLabel(Form)
        self.userNameLabel.setGeometry(QtCore.QRect(30, 20, 351, 16))
        self.userNameLabel.setWordWrap(True)
        self.userNameLabel.setObjectName("userNameLabel")
        self.userNameLabel.setStyleSheet("#userNameLabel{background-color: lightsteelblue}")
        self.horizontalLayoutWidget = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(30, 240, 371, 41))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        #字体按钮
        self.fontButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.fontButton.setObjectName("fontButton")
        self.fontButton.setIcon(QIcon('image/fonts.png'))
        self.horizontalLayout.addWidget(self.fontButton)

        #颜色按钮
        self.colorButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.colorButton.setObjectName("colorButton")
        self.colorButton.setIcon(QIcon('image/colors.png'))
        self.horizontalLayout.addWidget(self.colorButton)
        #文件发送按钮
        self.sendFileBtn  = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.sendFileBtn .setObjectName("sendFileBtn ")
        self.sendFileBtn.setIcon(QIcon('image/document.png'))
        self.horizontalLayout.addWidget(self.sendFileBtn )
        #截图按钮
        self.catchpicBtn = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.catchpicBtn.setObjectName("catchpicBtn")
        self.catchpicBtn.setIcon(QIcon('image/catchpic.png'))
        self.horizontalLayout.addWidget(self.catchpicBtn)
        #天气按钮
        self.weatherButton = QtWidgets.QToolButton(self.horizontalLayoutWidget)
        self.weatherButton.setObjectName("weatherButton")
        self.weatherButton.setIcon(QIcon('image/weather.png'))
        self.horizontalLayout.addWidget(self.weatherButton)

        #消息接收框
        self.messageBrowser = QtWidgets.QTextBrowser(Form)
        self.messageBrowser.setGeometry(QtCore.QRect(20, 60, 401, 171))
        self.messageBrowser.setObjectName("messageBrowser")
        self.messageBrowser.setStyleSheet("#messageBrowser{background-color: cornflowerblue}")
        #发送按钮
        self.sendButton = QtWidgets.QPushButton(Form)
        self.sendButton.setGeometry(QtCore.QRect(210, 400, 93, 28))
        self.sendButton.setObjectName("sendButton")
        self.sendButton.setIcon(QIcon('image/send.png'))
        #退出按钮
        self.quitButton = QtWidgets.QPushButton(Form)
        self.quitButton.setGeometry(QtCore.QRect(330, 400, 93, 28))
        self.quitButton.setObjectName("quitButton")
        self.quitButton.setIcon(QIcon('image/quit.png'))
        #表情按钮


        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "个人聊天"))
        self.userNameLabel.setText(_translate("Form", "当前用户信息："))
        self.fontButton.setText(_translate("Form", "字体"))
        self.colorButton.setText(_translate("Form", "颜色"))
        self.sendFileBtn .setText(_translate("Form", "文件传输"))
        self.catchpicBtn.setText(_translate("Form", "截图"))
        self.weatherButton.setText(_translate("Form","天气"))
        self.sendButton.setText(_translate("Form", "发送"))
        self.quitButton.setText(_translate("Form", "退出"))

if __name__ == '__main__':
    import sys
    ui = Ui_Form()
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    ui.setupUi(widget)
    widget.show()
    sys.exit(app.exec_())

