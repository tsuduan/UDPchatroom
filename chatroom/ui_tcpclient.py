from PyQt5 import QtCore, QtGui, QtWidgets
import sys
class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.tcpClientCancleBtn = QtWidgets.QPushButton(Form)
        self.tcpClientCancleBtn.setGeometry(QtCore.QRect(70, 210, 93, 28))
        self.tcpClientCancleBtn.setObjectName("tcpClientCancleBtn")
        self.tcpClientCloseBtn = QtWidgets.QPushButton(Form)
        self.tcpClientCloseBtn.setGeometry(QtCore.QRect(220, 210, 93, 28))
        self.tcpClientCloseBtn.setObjectName("tcpClientCloseBtn")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(90, 130, 211, 23))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.tcpClientStatusLabel = QtWidgets.QLabel(Form)
        self.tcpClientStatusLabel.setGeometry(QtCore.QRect(90, 60, 200, 16*4))
        self.tcpClientStatusLabel.setObjectName("tcpClientStatusLabel")
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tcpClientStatusLabel.sizePolicy().hasHeightForWidth())
        self.tcpClientStatusLabel.setWordWrap(True)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "等待文件接收"))
        self.tcpClientCancleBtn.setText(_translate("Form", "取消"))
        self.tcpClientCloseBtn.setText(_translate("Form", "关闭"))
        self.tcpClientStatusLabel.setText(_translate("Form", "文件接收Loading"))



if __name__ == '__main__':
    # 创建一个Qt应用对象
    ui = Ui_Form()
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    ui.setupUi(widget)
    widget.show()
    sys.exit(app.exec_())
