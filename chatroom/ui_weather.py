# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_weather.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.setStyleSheet("#Form{background-color: dodgerblue}")
        Form.setWindowIcon(QIcon('image/icecream1.png'))
        Form.resize(400, 359)
        self.weatherInfoImg = QtWidgets.QLabel(Form)
        self.weatherInfoImg.setGeometry(QtCore.QRect(140, 70, 121, 101))
        font = QtGui.QFont()
        font.setItalic(False)
        self.weatherInfoImg.setFont(font)
        self.weatherInfoImg.setMouseTracking(False)
        self.weatherInfoImg.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.weatherInfoImg.setText("")
        self.weatherInfoImg.setPixmap(QtGui.QPixmap("/weatherIcon/weatherInfo.png"))
        self.weatherInfoImg.setObjectName("weatherInfoImg")
        self.todayWeather = QtWidgets.QLabel(Form)
        self.todayWeather.setGeometry(QtCore.QRect(30, 60, 151, 81))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.todayWeather.setFont(font)
        self.todayWeather.setObjectName("todayWeather")
        self.todayAirQuality = QtWidgets.QLabel(Form)
        self.todayAirQuality.setGeometry(QtCore.QRect(30, 160, 131, 61))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.todayAirQuality.setFont(font)
        self.todayAirQuality.setWordWrap(False)
        self.todayAirQuality.setObjectName("todayAirQuality")
        self.airQualityImg = QtWidgets.QLabel(Form)
        self.airQualityImg.setGeometry(QtCore.QRect(170, 190, 31, 61))
        self.airQualityImg.setPixmap(QtGui.QPixmap("/weatherIcon/airQuality.png"))
        self.airQualityImg.setStyleSheet("#airQuality{background-color: dodgerblue}")
        self.airQualityImg.setText("")
        self.airQualityImg.setObjectName("airQualityImg")
        self.weatherTip = QtWidgets.QLabel(Form)
        self.weatherTip.setGeometry(QtCore.QRect(40, 250, 371, 81))
        self.weatherTip.setText("")
        self.weatherTip.setWordWrap(True)
        self.weatherTip.setObjectName("weatherTip")
        self.weatherInfo = QtWidgets.QLabel(Form)
        self.weatherInfo.setGeometry(QtCore.QRect(280, 130, 181, 21))
        self.weatherInfo.setObjectName("weatherInfo")
        self.city = QtWidgets.QLabel(Form)
        self.city.setGeometry(QtCore.QRect(20, 20, 181, 31))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.city.setFont(font)
        self.city.setObjectName("city")
        self.airQuality = QtWidgets.QLabel(Form)
        self.airQuality.setGeometry(QtCore.QRect(250, 210, 181, 16))
        self.airQuality.setText("")
        self.airQuality.setObjectName("airQuality")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "天气预报"))
        self.todayWeather.setText(_translate("Form", "今日天气："))
        self.todayAirQuality.setText(_translate("Form", "空气质量："))
        self.city.setText(_translate("Form", "当前城市："))



if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    win = Ui_Form()
    widget = QtWidgets.QWidget()
    win.setupUi(widget)
    widget.show()
    sys.exit(app.exec_())