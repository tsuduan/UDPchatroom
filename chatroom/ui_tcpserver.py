from PyQt5 import QtWidgets,QtCore
from PyQt5.QtGui import QIcon
class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        Form .setStyleSheet("#Form{background-color: orange}")
        Form.setWindowIcon(QIcon('image/sendFile.png'))

        self.serverOpenBtn = QtWidgets.QPushButton(Form)
        self.serverOpenBtn.setGeometry(QtCore.QRect(20, 210, 93, 28))
        self.serverOpenBtn.setObjectName("serverOpenBtn")
        self.serverCloseBtn = QtWidgets.QPushButton(Form)
        self.serverCloseBtn.setGeometry(QtCore.QRect(270, 210, 93, 28))
        self.serverCloseBtn.setObjectName("serverCloseBtn")
        self.progressBar = QtWidgets.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(90, 130, 211, 23))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.serverStatusLabel = QtWidgets.QLabel(Form)
        self.serverStatusLabel.setGeometry(QtCore.QRect(90, 30, 250, 90))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.serverStatusLabel.sizePolicy().hasHeightForWidth())
        self.serverStatusLabel.setWordWrap(True)
        self.serverStatusLabel.setObjectName("serverStatusLabel")
        self.serverSendBtn = QtWidgets.QPushButton(Form)
        self.serverSendBtn.setGeometry(QtCore.QRect(140, 210, 93, 28))
        self.serverSendBtn.setObjectName("serverSendBtn")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "文件发送"))
        self.serverOpenBtn.setText(_translate("Form", "打开"))
        self.serverCloseBtn.setText(_translate("Form", "关闭"))
        self.serverStatusLabel.setText(_translate("Form", "文件发送Loading"))
        self.serverSendBtn.setText(_translate("Form", "发送"))



if __name__ == '__main__':
    import sys
    ui = Ui_Form()
    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    ui.setupUi(widget)
    widget.show()
    sys.exit(app.exec_())
